#pragma once

#include <limits>
#include <vector>
#include <string>
#include <memory>
#include <xmemory>
#include <algorithm>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stddef.h>
#include <type_traits>
#include <xkeycheck.h>
#include <filesystem>


#include <opencv2/opencv.hpp>


#include <SDL.h>

// About Desktop OpenGL function loaders:
//  Modern desktop OpenGL doesn't have a standard portable header file to load OpenGL function pointers.
//  Helper libraries are often used for this purpose! Here we are supporting a few common ones (gl3w, glew, glad).
//  You may use another loader/header of your choice (glext, glLoadGen, etc.), or chose to manually implement your own.
#include "gl3w.h"    // Initialize with gl3wInit()



#include "imgui.h"
#include "imgui_impl_sdl.h"
#include "imgui_impl_opengl3.h"

#include "dcmtk/dcmdata/dctk.h"
#include "dcmtk/dcmimgle/dcmimage.h"

#include "UI.h"

#include <cstdlib>
#include <thread>
#include "tinyfiledialogs.h"


// use this instead of directly using imshow, Release mode won't include imshow (IMSHOW_ENABLED is defined in debug mode)
#ifdef IMSHOW_ENABLED
	#define IMSHOW(name,mat) {cv::imshow(name,mat);}
#else
	#define IMSHOW(name,mat)
#endif