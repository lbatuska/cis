#include "pch.h"

#include "Filters.h"

namespace Filters
{
	auto BasicThresholdingFunc(MedicalInfoWindow* ptr) -> void
	{
		if (ptr->thresholding._thresholding)
		{
			cv::threshold(ptr->mat_img_modified_to_show,
				ptr->mat_img_modified_to_show,
				ptr->thresholding._threshold_value,
				255,
				ptr->thresholding._threshold_type);
		}
	}

	auto HoughCircles(MedicalInfoWindow* ptr) -> void
	{
		cv::HoughCircles(ptr->mat_img_modified_to_show,
			ptr->houghcircles.circles,
			cv::HOUGH_GRADIENT,
			1,
			ptr->mat_img_modified_to_show.rows / ptr->houghcircles.min_dist_divisor,
			ptr->houghcircles.param_1,
			ptr->houghcircles.param_2,
			ptr->houghcircles.min_radius,
			ptr->houghcircles.max_radius);
		for (size_t i = 0; i < ptr->houghcircles.circles.size(); i++)
		{
			cv::Point center = cv::Point(ptr->houghcircles.circles[i][0], ptr->houghcircles.circles[i][1]);
			cv::circle(ptr->mat_img_modified_to_show, center, ptr->houghcircles.circles[i][2], cv::Scalar(255, 0, 255), 3, cv::LINE_AA);
		}
	}

	auto Histogram(MedicalInfoWindow* ptr) -> void
	{
		int histSize = 256;
		float range[] = { 0, 256 }; // the upper boundary is exclusive
		const float* histRange = { range };
		bool uniform = true, accumulate = false;

		cv::Mat plane;

		cv::calcHist(&ptr->mat_img_modified_to_show, 1, 0, cv::Mat(), plane, 1, &histSize, &histRange, uniform, accumulate);
		int hist_w = 512;
		int hist_h = 256;
		int bin_w = cvRound((double)hist_w / histSize);

		cv::Mat histImage(hist_h, hist_w, CV_8UC1, cv::Scalar(0, 0, 0));

		cv::normalize(plane, plane, 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat());


		for (int i = 1; i < histSize; i++)
		{
			line(histImage, cv::Point(bin_w * (i - 1), hist_h - cvRound(plane.at<float>(i - 1))),
				cv::Point(bin_w * (i), hist_h - cvRound(plane.at<float>(i))),
				cv::Scalar(255, 0, 0), 2, 8, 0);
		}
		IMSHOW("calcHist Demo", histImage)
	}

	// not working yet ... 
	auto Segmentation(MedicalInfoWindow* ptr) -> void
	{
		std::cout << "Segmentation is WIP, causes exception!\n";
		return;

		cv::Mat toFilter;
		ptr->mat_img_modified_to_show.copyTo(toFilter);
		std::cout << toFilter.channels();
		/*
		// toFilter.convertTo(toFilter, CV_8UC3);
		// Change the background from white to black, since that will help later to extract
		// better results during the use of Distance Transform
		for (int i = 0; i < toFilter.rows; i++)
		{
			for (int j = 0; j < toFilter.cols; j++)
			{
				if (toFilter.at<cv::Vec3b>(i, j) == cv::Vec3b(255, 255, 255))
				{
					toFilter.at<cv::Vec3b>(i, j)[0] = 0;
					toFilter.at<cv::Vec3b>(i, j)[1] = 0;
					toFilter.at<cv::Vec3b>(i, j)[2] = 0;
				}
			}
		}
		*/
		IMSHOW("Source Image", toFilter)
		// Create a kernel that we will use to sharpen our image
		cv::Mat kernel = (cv::Mat_<float>(3, 3) <<
			1, 1, 1,
			1, -8, 1,
			1, 1, 1); // an approximation of second derivative, a quite strong kernel
		// do the laplacian filtering as it is
		// well, we need to convert everything in something more deeper then CV_8U
		// because the kernel has some negative values,
		// and we can expect in general to have a Laplacian image with negative values
		// BUT a 8bits unsigned int (the one we are working with) can contain values from 0 to 255
		// so the possible negative number will be truncated
		cv::Mat imgLaplacian;
		cv::filter2D(toFilter, imgLaplacian, CV_32F, kernel);
		cv::Mat sharp;
		toFilter.convertTo(sharp, CV_32F);
		cv::Mat imgResult = sharp - imgLaplacian;
		// convert back to 8bits gray scale
		imgResult.convertTo(imgResult, CV_8UC3);
		imgLaplacian.convertTo(imgLaplacian, CV_8UC3);
		cv::imshow("New Sharped Image", imgResult);


		// Create binary image from source image
		cv::Mat bw(imgResult.size(), CV_8UC3);
		// cv::cvtColor(imgResult, bw, cv::COLOR_BGR2GRAY);
		cv::threshold(imgResult, bw, 40, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
		IMSHOW("Binary Image", bw)


		// Perform the distance transform algorithm
		cv::Mat dist;
		cv::distanceTransform(imgResult, dist, cv::DIST_L2, 3);
		// Normalize the distance image for range = {0.0, 1.0}
		// so we can visualize and threshold it
		cv::normalize(dist, dist, 0, 1.0, cv::NORM_MINMAX);
		IMSHOW("Distance Transform Image", dist)

		// Threshold to obtain the peaks
		// This will be the markers for the foreground objects
		cv::threshold(dist, dist, 0.4, 1.0, cv::THRESH_BINARY);
		// Dilate a bit the dist image
		cv::Mat kernel1 = cv::Mat::ones(3, 3, CV_8U);
		cv::dilate(dist, dist, kernel1);
		IMSHOW("Peaks", dist)
		// working up to this point ------------------------------------------------------

		// Create the CV_8U version of the distance image
		// It is needed for findContours()
		cv::Mat dist_8u;
		dist.convertTo(dist_8u, CV_8U);
		// Find total markers
		std::vector<std::vector<cv::Point> > contours;
		cv::findContours(dist_8u, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
		// Create the marker image for the watershed algorithm
		cv::Mat markers = cv::Mat::zeros(dist.size(), CV_32S);
		// Draw the foreground markers
		std::cout << contours.size();
		for (int i = 0; i < contours.size(); i++)
		{
			cv::drawContours(markers, contours, i, cv::Scalar(i + 1), -1);
		}
		// Draw the background marker
		cv::circle(markers, cv::Point(5, 5), 3, cv::Scalar(255), -1);
		IMSHOW("Markers", markers * 10000)


		// Perform the watershed algorithm
		cv::watershed(imgResult, markers);
		cv::Mat mark;
		markers.convertTo(mark, CV_8U);
		cv::bitwise_not(mark, mark);
		IMSHOW("Markers_v2", mark) // uncomment this if you want to see how the mark
		// image looks like at that point
		// Generate random colors
		std::vector<cv::Vec3b> colors;
		for (size_t i = 0; i < contours.size(); i++)
		{
			int b = cv::theRNG().uniform(0, 256);
			int g = cv::theRNG().uniform(0, 256);
			int r = cv::theRNG().uniform(0, 256);
			colors.push_back(cv::Vec3b((uchar)b, (uchar)g, (uchar)r));
		}
		// Create the result image
		cv::Mat dst = cv::Mat::zeros(markers.size(), CV_8UC3);
		// Fill labeled objects with random colors
		for (int i = 0; i < markers.rows; i++)
		{
			for (int j = 0; j < markers.cols; j++)
			{
				int index = markers.at<int>(i, j);
				if (index > 0 && index <= static_cast<int>(contours.size()))
				{
					dst.at<cv::Vec3b>(i, j) = colors[index - 1];
				}
			}
		}
		// Visualize the final image
		IMSHOW("Final Result", dst)

	}

	// only good for colored images... 
	auto ImageEnhancement(MedicalInfoWindow* ptr) -> void
	{
		cv::Mat toFilter;
		ptr->mat_img_modified_to_show.copyTo(toFilter);
		IMSHOW("this", toFilter)
		double alpha = 2.2;
		int beta = 50;

		for (int y = 0; y < toFilter.rows; y++)
		{
			for (int x = 0; x < toFilter.cols; x++)
			{
				for (int c = 0; c < 3; c++)
				{
					toFilter.at<cv::Vec3b>(y, x)[c] = cv::saturate_cast<uchar>(alpha * (toFilter.at<cv::Vec3b>(y, x)[c]) + beta);
				}
			}
		}
		IMSHOW("image", toFilter)
	}

	auto EqualizeHistogram(MedicalInfoWindow* ptr) -> void
	{
		// cv::Mat toFilter;

		cv::equalizeHist(ptr->mat_img_modified_to_show, ptr->mat_img_modified_to_show);
		// ptr->mat_img_modified_to_show.copyTo(toFilter);

	// IMSHOW("this", toFilter)

	// cv::cvtColor(toFilter, toFilter, cv::COLOR_BGR2GRAY);
	// cv::equalizeHist(toFilter, toFilter);
	// IMSHOW("image", toFilter)
	// toFilter.copyTo(ptr->mat_img_modified_to_show);
	}

	auto CannyEdgeDetection(MedicalInfoWindow* ptr) -> void
	{
		cv::Mat toFilter;

		ptr->mat_img_modified_to_show.copyTo(toFilter);

		IMSHOW("this", toFilter)
		cv::Mat detected_edges;
		cv::Mat dest;
		toFilter.copyTo(dest);

		cv::blur(toFilter, detected_edges, cv::Size(3, 3));
		cv::Canny(detected_edges, detected_edges, ptr->canny.lowThreshold, 3 * ptr->canny.lowThreshold, 3); // input these values with sliders

		dest = cv::Scalar::all(0);
		toFilter.copyTo(dest, detected_edges);
		IMSHOW("this is", dest)
		dest.copyTo(ptr->mat_img_modified_to_show);
	}


	auto GradientEdgeDetection(MedicalInfoWindow* ptr) -> void
	{
		cv::Mat toFilter;

		ptr->mat_img_modified_to_show.copyTo(toFilter);

		IMSHOW("this", toFilter)
		cv::Mat grad;

		cv::GaussianBlur(toFilter, toFilter, cv::Size(3, 3), 0, 0, cv::BORDER_DEFAULT);
		// convert to grayscale (already in grayscale)
		cv::Mat grad_x, grad_y;
		cv::Mat abs_grad_x, abs_grad_y;

		cv::Sobel(toFilter, grad_x, CV_16S, 1, 0, ptr->grad.ksize, ptr->grad.scale, ptr->grad.delta, cv::BORDER_DEFAULT); // input with sliders
		cv::Sobel(toFilter, grad_y, CV_16S, 0, 1, ptr->grad.ksize, ptr->grad.scale, ptr->grad.delta, cv::BORDER_DEFAULT); // input with sliders

		cv::convertScaleAbs(grad_x, abs_grad_x);
		cv::convertScaleAbs(grad_y, abs_grad_y);

		cv::addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);
		toFilter.copyTo(ptr->mat_img_modified_to_show);
		IMSHOW("this", grad)
	}

	auto Dilate(MedicalInfoWindow* ptr) -> void
	{
		int dilation_type = 0;
		if (ptr->dilation.dilation_type == 1)
		{
			dilation_type = cv::MORPH_RECT;
		}
		else if (ptr->dilation.dilation_type == 2)
		{
			dilation_type = cv::MORPH_CROSS;
		}
		else if (ptr->dilation.dilation_type == 3)
		{
			dilation_type = cv::MORPH_ELLIPSE;
		}

		cv::Mat toFilter;
		ptr->mat_img_modified_to_show.copyTo(toFilter);
		IMSHOW("this", toFilter)

		cv::Mat element = cv::getStructuringElement(dilation_type, cv::Size(2 * ptr->dilation.size + 1, 2 * ptr->dilation.size + 1), cv::Point(ptr->dilation.size, ptr->dilation.size)); // input shape (MORPH_RECT), input size
		cv::dilate(toFilter, toFilter, element);
		toFilter.copyTo(ptr->mat_img_modified_to_show);
		IMSHOW("this_dilate", toFilter)
	}

	auto Erode(MedicalInfoWindow* ptr) -> void
	{
		int erosion_type = 0;
		if (ptr->erosion.erosion_type == 1)
		{
			erosion_type = cv::MORPH_RECT;
		}
		else if (ptr->erosion.erosion_type == 2)
		{
			erosion_type = cv::MORPH_CROSS;
		}
		else if (ptr->erosion.erosion_type == 3)
		{
			erosion_type = cv::MORPH_ELLIPSE;
		}

		cv::Mat toFilter;
		ptr->mat_img_modified_to_show.copyTo(toFilter);
		IMSHOW("this", toFilter)

		cv::Mat element = cv::getStructuringElement(erosion_type, cv::Size(2 * ptr->erosion.size + 1, 2 * ptr->erosion.size + 1), cv::Point(ptr->erosion.size, ptr->erosion.size)); // input shape (MORPH_RECT), input size
		cv::erode(toFilter, toFilter, element);
		toFilter.copyTo(ptr->mat_img_modified_to_show);
		IMSHOW("this_erode", toFilter)
	}

	auto Open(MedicalInfoWindow* ptr) -> void
	{
		int dilation_type = 0;
		if (ptr->dilation.dilation_type == 1)
		{
			dilation_type = cv::MORPH_RECT;
		}
		else if (ptr->dilation.dilation_type == 2)
		{
			dilation_type = cv::MORPH_CROSS;
		}
		else if (ptr->dilation.dilation_type == 3)
		{
			dilation_type = cv::MORPH_ELLIPSE;
		}

		cv::Mat toFilter;
		ptr->mat_img_modified_to_show.copyTo(toFilter);
		IMSHOW("this", toFilter)

		cv::Mat element = cv::getStructuringElement(dilation_type, cv::Size(2 * ptr->open.size + 1, 2 * ptr->open.size + 1), cv::Point(ptr->open.size, ptr->open.size)); // input shape (MORPH_RECT), input size
		cv::morphologyEx(toFilter, toFilter, cv::MORPH_OPEN, element);
		toFilter.copyTo(ptr->mat_img_modified_to_show);
		IMSHOW("this_open", toFilter)
	}

	auto Close(MedicalInfoWindow* ptr) -> void
	{
		int dilation_type = 0;
		if (ptr->dilation.dilation_type == 1)
		{
			dilation_type = cv::MORPH_RECT;
		}
		else if (ptr->dilation.dilation_type == 2)
		{
			dilation_type = cv::MORPH_CROSS;
		}
		else if (ptr->dilation.dilation_type == 3)
		{
			dilation_type = cv::MORPH_ELLIPSE;
		}

		cv::Mat toFilter;
		ptr->mat_img_modified_to_show.copyTo(toFilter);
		IMSHOW("this", toFilter)

		cv::Mat element = cv::getStructuringElement(dilation_type, cv::Size(2 * ptr->close.size + 1, 2 * ptr->close.size + 1), cv::Point(ptr->close.size, ptr->close.size)); // input shape (MORPH_RECT), input size
		cv::morphologyEx(toFilter, toFilter, cv::MORPH_CLOSE, element);
		toFilter.copyTo(ptr->mat_img_modified_to_show);
		IMSHOW("this_close", toFilter)
	}
};