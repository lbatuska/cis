#include <iostream>
#include <vector>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

#include <SDL.h>

#include "imgui.h"
//#include "imgui_impl_sdl_gl3.h"

#include "UI.h"

#include "ImageWindow.h"


int not_main(int argc, char** argv)
{

	ImageWindow IW("asd",
		ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_HorizontalScrollbar,
		ImVec2(20, 20), ImVec2(1000, 1000));

	int WindowWidth = 1280;
	int WindowHeight = 720;

	SDL_Window* window = UI::Init("Test Window", WindowWidth, WindowHeight);

	// image
	cv::Mat frame = cv::imread("stock.jpg");
	GLuint frameTexture = UI::MatToTex(frame);
	// image

	// vid
	cv::VideoCapture video;
	video.open(0);
	cv::Mat videoFrame;
	GLuint videoFrameTexture;
	// vid

	Uint32 old_time = SDL_GetTicks();
	Uint32 new_time;
	float seconds_passed;
	float milli_seconds_passed;
	float FPS = 25;

	bool showMainWindow = true;

	bool finished = false;
	while (!finished)
	{
		// Handle Events
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			ImGui_ImplSdlGL3_ProcessEvent(&event);
			switch (event.type)
			{
			case SDL_QUIT:
				finished = true;
				break;
			case SDL_MOUSEMOTION:
				/*ui::on_mouse_motion(event);
				if (dragging)
					rect_pt2 = ui::mouse_pos;*/
				break;
			case SDL_MOUSEBUTTONDOWN:
				/*ui::on_mouse_button_down(event);
				if (ui::mouse_button_states[0])
				{
					dragging = true;
					rect_pt1 = ui::mouse_pos;
					rect_pt2 = ui::mouse_pos;
				}*/
				break;
			case SDL_MOUSEBUTTONUP:
				/*ui::on_mouse_button_up(event);
				if (!ui::mouse_button_states[0])
				{
					dragging = false;
					rect_pt2 = ui::mouse_pos;
				}*/
				break;
			default:
				break;
			}
		}

		// code comes here -----------------------------------------------------

		new_time = SDL_GetTicks();
		milli_seconds_passed = (float)(new_time - old_time);
		seconds_passed = (float)(new_time - old_time) / 1000;

		// Background Window
		ImGui_ImplSdlGL3_NewFrame(window);

		// we should update frames
		glDeleteTextures(1, &frameTexture);
		frameTexture = UI::MatToTex(frame);

		//ImGui::SetNextWindowSize(ImVec2(100, 100));
		ImGui::SetNextWindowSizeConstraints(ImVec2(100, 100), ImVec2(500, 500));
		ImGui::Begin("Main Window", &showMainWindow,
			ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_HorizontalScrollbar);

		// here comes the ui part

		//ImGui::Checkbox("Test Check Box", &showMainWindow);
		ImGui::Text(("Frame time(ms) : "+std::to_string(milli_seconds_passed)).c_str());
		ImGui::Text(("Fps : "+std::to_string(1/seconds_passed)).c_str());


		ImGui::End();


		ImGui::SetNextWindowSizeConstraints(ImVec2(100, 100), ImVec2(500, 500));
		ImGui::Begin("IMAGE", &showMainWindow,
			ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_HorizontalScrollbar);

		// here comes the ui part

		ImGui::Image((void*)frameTexture, ImVec2(frame.size().width,
			frame.size().height));

		ImGui::End();


		video.read(videoFrame);
		glDeleteTextures(1, &videoFrameTexture);
		videoFrameTexture = UI::MatToTex(videoFrame);
		ImGui::SetNextWindowSizeConstraints(ImVec2(100, 100), ImVec2(800, 600));
		ImGui::Begin("VIDEO", &showMainWindow,
			ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_HorizontalScrollbar);

		// here comes the ui part

		ImGui::Image((void*)videoFrameTexture, ImVec2(videoFrame.size().width,
			videoFrame.size().height));

		ImGui::End();

		IW.DrawWindow(videoFrame);

		UI::RenderWindow(window);
		old_time = new_time;
		// code comes here -----------------------------------------------------

	}


	{
		//// Read the image file
		//cv::Mat image = cv::imread("stock.jpg");

		//if (image.empty()) // Check for failure
		//{
		//	std::cout << "Could not open or find the image" << std::endl;
		//	system("pause"); //wait for any key press
		//	return -1;
		//}

		//std::cout << "Image found! Displaying it in : 1024x768!" << std::endl;

		//cv::resize(image, image, cv::Size(1024, 768));

		//std::string windowName = "Test Window"; //Name of the window

		//cv::namedWindow(windowName); // Create a window

		//imshow(windowName, image); // Show our image inside the created window.

		//cv::waitKey(0); // Wait for any keystroke in the window

		//cv::destroyWindow(windowName); //destroy the created window
	}

	UI::Cleanup(window);

	return 0;
}