#include "pch.h"

#include "MedicalInfoWindow.h"

////////////////////////////////////////////////////////////////////////////////
// All available DCMTK libraries are listed here                             //
// If you wish to use something that was originally commented out           //
// Make sure to manually copy the right lib file from the DCMTK\lib folder //
// To the forlder corresponding to your build x64\Debug or x64\Release    //
///////////////////////////////////////////////////////////////////////////

 //#pragma comment( lib, "charls.lib" )
 //#pragma comment( lib, "cmr.lib" )
#pragma comment( lib, "dcmdata.lib" )
//#pragma comment( lib, "dcmdsig.lib" )
//#pragma comment( lib, "dcmect.lib" )
//#pragma comment( lib, "dcmfg.lib" )
#pragma comment( lib, "dcmimage.lib" )
#pragma comment( lib, "dcmimgle.lib" )
//#pragma comment( lib, "dcmiod.lib" )
//#pragma comment( lib, "dcmjpeg.lib" )
//#pragma comment( lib, "dcmjpls.lib" )
//#pragma comment( lib, "dcmnet.lib" )
//#pragma comment( lib, "dcmpmap.lib" )
//#pragma comment( lib, "dcmpstat.lib" )
//#pragma comment( lib, "dcmqrdb.lib" )
//#pragma comment( lib, "dcmrt.lib" )
//#pragma comment( lib, "dcmseg.lib" )
//#pragma comment( lib, "dcmsr.lib" )
//#pragma comment( lib, "dcmtls.lib" )
//#pragma comment( lib, "dcmtract.lib" )
//#pragma comment( lib, "dcmwlm.lib" )
//#pragma comment( lib, "i2d.lib" )
//#pragma comment( lib, "ijg8.lib" )
//#pragma comment( lib, "ijg12.lib" )
//#pragma comment( lib, "ijg16.lib" )
#pragma comment( lib, "oflog.lib" )
#pragma comment( lib, "ofstd.lib" )

static std::thread t;


const char* folderPath = NULL;
char const* selectedfolderPath;
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// -------------------- tinyfiledialogs function Begin ----------------------------------------------------------------//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void* call_from_thread(void*) {
	selectedfolderPath = tinyfd_selectFolderDialog(
		"Select Folder with DICOM files in it", ""
	);
	if (!selectedfolderPath)
	{
		tinyfd_messageBox(
			"Error",
			"Unexpected problem while selecting folder",
			"ok",
			"error",
			1);
		return NULL;
	}
	else
	{
		folderPath = selectedfolderPath;
	}
	return NULL;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// -------------------- tinyfiledialogs function End ------------------------------------------------------------------//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
auto main(int, char**) -> int
{
	SDL_Window* _window = UI::Init("Test", 1280, 720);
	if (_window == nullptr)
	{
		return -1;
	}
	// Keep the logging here until it's cleared or put to the console
	std::ostringstream _oss;
	bool _always_log_to_console = true;

	bool _folder_window = true;

	constexpr int _frame_times_count = 500;
	float _frame_times[_frame_times_count] = { 0 };
	float _ms_per_frame = 0;
	bool _frame_times_plot_enabled = true;


	std::vector<std::shared_ptr<MedicalInfoWindow>> _medinfo_windows_loaded;

	// Indicates to the program that SDL received a quit event, terminates main loop
	bool _done = false;

	SDL_Event _event;

	while (!_done)
	{
		// Poll and handle events (inputs, window resize, etc.)
		// You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
		// - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
		// - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
		// Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
		while (SDL_PollEvent(&_event))
		{
			ImGui_ImplSDL2_ProcessEvent(&_event);
			if (_event.type == SDL_QUIT)
				_done = true;
			if (_event.type == SDL_WINDOWEVENT && _event.window.event == SDL_WINDOWEVENT_CLOSE && _event.window.windowID == SDL_GetWindowID(_window))
				_done = true;
		}

		// Start the Dear ImGui frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplSDL2_NewFrame(_window);
		ImGui::NewFrame();

		// Render the main window
			 /////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// -------------------- Main Window Begin -----------------------------------------------------------------//
		   /////////////////////////////////////////////////////////////////////////////////////////////////////////////
		ImGui::Begin("Main Window");

		ImGui::Checkbox("Show Folder Loader", &_folder_window);


		_ms_per_frame = 1000.0f / ImGui::GetIO().Framerate;
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", _ms_per_frame, ImGui::GetIO().Framerate);

		ImGui::Checkbox("Frame Times Plot Update", &_frame_times_plot_enabled);

		if (_frame_times_plot_enabled)
		{
			for (size_t i = 0; i < _frame_times_count - 1; i++)
			{
				_frame_times[i] = _frame_times[i + 1];
			}
			_frame_times[_frame_times_count - 1] = _ms_per_frame;

			ImGui::PlotLines("Frame time in ms", _frame_times, _frame_times_count);
		}

		ImGui::End();
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	   // -------------------- Main Window End -----------------------------------------------------------------------//
	  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	   // -------------------- Load Folder Window Begin --------------------------------------------------------------//
	  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if (_folder_window)
		{
			ImGui::Begin("Load Folder", &_folder_window);
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		   // -------------------- tinyfiledialogs Begin -----------------------------------------------------------------//
		  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if (ImGui::Button("Select Folder"))
			{
				selectedfolderPath = NULL;
				folderPath = NULL;
				// If the dialog is open and we try to open it again, stop the program until it is closed
				if (t.joinable())
				{
					t.join();
					t = std::thread(&call_from_thread, *call_from_thread);
				}
				else
				{
					t = std::thread(&call_from_thread, *call_from_thread);
				}
			}
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		   // -------------------- tinyfiledialogs End -------------------------------------------------------------------//
		  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			ImGui::SameLine();
			if (ImGui::Button("Load Files From Folder"))
			{
				if (t.joinable())
				{
					t.join();
				}
				if (folderPath != NULL)
				{
					// Destruct and remove all elements loaded before
					if (_medinfo_windows_loaded.size() > 0)
					{
						for (size_t i = 0; i < _medinfo_windows_loaded.size(); i++)
						{
							_medinfo_windows_loaded[i]->info_window_enabled_ = false;
							std::cout << "Unloading : " << _medinfo_windows_loaded[i]->GetFilePath() << "\n";
						}
						/*for (const auto x : _medinfo_windows_loaded)
						{
							x->info_window_enabled_ = false;
						}*/
						_medinfo_windows_loaded.clear();
					}

					for (const auto& entry : std::filesystem::directory_iterator(folderPath))
					{
						std::string pth = entry.path().string();

						_oss << "Opening : " << entry.path() << " -> ";
						auto x = std::make_shared<MedicalInfoWindow>(pth.c_str());

						if (x->_open_dicom_successful)
						{
							_oss << "Successful!\n";
							x->SetWindowAndImageWindowName(pth.c_str(),
								std::string(pth + " Image").c_str());
							_medinfo_windows_loaded.push_back(std::move(x));
						}
						else
						{
							_oss << "Unsuccessful!\n";
						}
						if (_always_log_to_console)
						{
							std::cout << _oss.str();
							_oss.str(std::string());
						}
					}
					selectedfolderPath = NULL;
					folderPath = NULL;
				}
			}
			ImGui::SameLine();
			if (ImGui::Button("Unload All Data"))
			{
				// Destruct and remove all elements loaded before
				if (_medinfo_windows_loaded.size() > 0)
				{
					for (size_t i = 0; i < _medinfo_windows_loaded.size(); i++)
					{
						_medinfo_windows_loaded[i]->info_window_enabled_ = false;
						std::cout << "Unloading : " << _medinfo_windows_loaded[i]->GetFilePath() << "\n";
					}
					/*for (const auto x : _medinfo_windows_loaded)
					{
						x->info_window_enabled_ = false;
						std::cout << "Unloading : " << x->GetFilePath() << "\n";
					}*/
					_medinfo_windows_loaded.clear();
				}
			}
			ImGui::Checkbox("Log File Events To Console", &_always_log_to_console);
			ImGui::SameLine();
			if (ImGui::Button("Dump Log To Console"))
			{
				std::cout << _oss.str();
				_oss.str(std::string());
			}
			ImGui::SameLine();
			if (!_always_log_to_console)
			{
				if (ImGui::Button("Clear Log"))
				{
					_oss.str(std::string());
				}
			}
			ImGui::NewLine();
			for (size_t i = 0; i < _medinfo_windows_loaded.size(); i++)
			{
				ImGui::Checkbox(_medinfo_windows_loaded[i]->GetWindowName(), &(_medinfo_windows_loaded[i]->info_window_enabled_));
			}
			ImGui::End();
		}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	   // -------------------- Load Folder Window End ----------------------------------------------------------------//
	  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	   // Render each medical image window if they are enabled (info_window_enabled_)
		for (size_t i = 0; i < _medinfo_windows_loaded.size(); i++)
		{
			_medinfo_windows_loaded[i]->DrawWindow();
		}

		UI::RenderWindow(_window);

	}
	// Wait for the thread to close before exiting
	if (t.joinable())
	{
		t.join();
	}
	// Close OpenGL context and clean up before exiting
	UI::Cleanup(_window);
	// Wait for input in case we want to read the consol info
	std::cin.get();
	return 0;
}
