#include "pch.h"

namespace UI
{
	std::vector<bool> _mouse_button_states(3, false);
	cv::Point _mouse_pos(0, 0);

	ImGuiIO* _io;


	GLint _swizzle_mask[] = { GL_RED, GL_RED, GL_RED, GL_ONE };

	SDL_GLContext GLContext;

#if defined(__cpp_concepts) && _HAS_CXX20
	template<integer Int>
	SDL_Window* Init(const char* windowname, Int width, Int height)
#else
	SDL_Window* Init(const char* windowname, int width, int height)
#endif
	{
		// Setup SDL
		// (Some versions of SDL before <2.0.10 appears to have performance/stalling issues on a minority of Windows systems,
		// depending on whether SDL_INIT_GAMECONTROLLER is enabled or disabled.. updating to latest version of SDL is recommended!)
		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0)
		{
			printf("Error: %s\n", SDL_GetError());
			return nullptr;
		}


		const int vers[4][2] = { {4,5},{4,0},{3,5},{3,0} };

		const char* glsl_version = "#version 130";
		bool _err;
		SDL_WindowFlags _window_flags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
		SDL_Window* _window = nullptr;
		SDL_GLContext _gl_context = nullptr;

		for (int i = 0; i < sizeof(vers) / 2; i++)
		{

			SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, vers[i][0]);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, vers[i][1]);

			// Create window with graphics context
			SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
			SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
			SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
			//SDL_WindowFlags _window_flags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
			_window = SDL_CreateWindow(windowname, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, _window_flags);
			_gl_context = SDL_GL_CreateContext(_window);
			SDL_GL_MakeCurrent(_window, _gl_context);
			SDL_GL_SetSwapInterval(0); // Enable vsync

			// Initialize OpenGL loader
			_err = gl3wInit() != 0;


			if (!_err)
			{
				break;
			}
				std::cout << "OpenGL " << vers[i][0] << "." << vers[i][1] << " is not supported on your system!\n";
				SDL_GL_DeleteContext(GLContext);
				SDL_DestroyWindow(_window);

		}

		if (_err)
		{
			fprintf(stderr, "Failed to initialize OpenGL loader!\nMinimum OpenGL 3.0 is required!\n");
			SDL_GL_DeleteContext(GLContext);
			SDL_DestroyWindow(_window);
			SDL_Quit();
			return nullptr;
		}

		std::cout << glGetString(GL_VERSION) << "\n";

		// Setup Dear ImGui context
		IMGUI_CHECKVERSION();
		ImGui::CreateContext();
		_io = &ImGui::GetIO(); (void)_io;
		//ImGuiIO& io = *_io;
		//ImGuiIO& io = ImGui::GetIO(); (void)io;

		_io->ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
		//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
		_io->ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
		_io->ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows
		//io.ConfigViewportsNoAutoMerge = true;
		//io.ConfigViewportsNoTaskBarIcon = true;

		// Setup Dear ImGui style
		ImGui::StyleColorsDark();
		//ImGui::StyleColorsClassic();

		// When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
		ImGuiStyle& style = ImGui::GetStyle();
		if (_io->ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
		{
			style.WindowRounding = 0.0f;
			style.Colors[ImGuiCol_WindowBg].w = 1.0f;
		}

		// Setup Platform/Renderer bindings
		ImGui_ImplSDL2_InitForOpenGL(_window, _gl_context);
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// These lines of fprintf has been commented out in imgui_impl_opengl3.cpp to avoid unnecessary cluttering of the console window   //
		// 474                                                                                                                            //
		// 492                                                                                                                           //
		// These lines of fprintf has been commented out in imgui_impl_opengl3.cpp to avoid unnecessary cluttering of the console window//
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		ImGui_ImplOpenGL3_Init(glsl_version);

		// Load Fonts
		// - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
		// - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
		// - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
		// - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
		// - Read 'docs/FONTS.txt' for more instructions and details.
		// - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
		//io.Fonts->AddFontDefault();
		//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
		//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
		//io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
		//io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
		//ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
		//IM_ASSERT(font != NULL);


		return _window;

	}

	void RenderWindow(SDL_Window* window, ImVec4 clear_color)
	{

		// Rendering
		//ImGuiIO& io = *_io;
		ImGui::Render();
		glViewport(0, 0, (int)_io->DisplaySize.x, (int)_io->DisplaySize.y);
		glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
		glClear(GL_COLOR_BUFFER_BIT);
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		// Update and Render additional Platform Windows
		// (Platform functions may change the current OpenGL context, so we save/restore it to make it easier to paste this code elsewhere.
		//  For this specific demo app we could also call SDL_GL_MakeCurrent(window, gl_context) directly)
		if (_io->ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
		{
			SDL_Window* _backup_current_window = SDL_GL_GetCurrentWindow();
			SDL_GLContext _backup_current_context = SDL_GL_GetCurrentContext();
			ImGui::UpdatePlatformWindows();
			ImGui::RenderPlatformWindowsDefault();
			SDL_GL_MakeCurrent(_backup_current_window, _backup_current_context);
		}

		SDL_GL_SwapWindow(window);
	}

	GLuint MatToTex(const cv::Mat& mat)
	{
		//std::cout << "MatToTex called " << mat.total() * mat.elemSize() << " Bytes, Is Continuous:" << mat.isContinuous() << " "<< (mat.total() * mat.elemSize() )/1048576.0f <<"Mb" << std::endl;
		GLuint _tex_id;
		glGenTextures(1, &_tex_id);

		glBindTexture(GL_TEXTURE_2D, _tex_id);
		// Set texture interpolation methods for minification and magnification
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		// Set texture clamping method
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		// Create the texture
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, mat.cols, mat.rows, 0, GL_BGR,
			GL_UNSIGNED_BYTE, mat.ptr());

		return _tex_id;
	}

	GLuint MatToTex1C(const cv::Mat& mat)
	{
		//std::cout << "MatToTex called " << mat.total() * mat.elemSize() << " Bytes, Is Continuous:" << mat.isContinuous() << " "<< (mat.total() * mat.elemSize() )/1048576.0f <<"Mb" << std::endl;
		GLuint _tex_id;
		glGenTextures(1, &_tex_id);

		glBindTexture(GL_TEXTURE_2D, _tex_id);
		// Set texture interpolation methods for minification and magnification
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		// Set texture clamping method
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		// swizzling
		glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, _swizzle_mask);

		// Create the texture
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mat.cols, mat.rows, 0, GL_RED,
			GL_UNSIGNED_BYTE, mat.ptr());

		return _tex_id;
	}

	void Cleanup(SDL_Window* window)
	{
		// Cleanup
		ImGui_ImplOpenGL3_Shutdown();
		ImGui_ImplSDL2_Shutdown();
		ImGui::DestroyContext();

		SDL_GL_DeleteContext(GLContext);
		SDL_DestroyWindow(window);
		SDL_Quit();
	}

	void OnMouseMotion(SDL_Event& event)
	{
		_mouse_pos.x = event.motion.x;
		_mouse_pos.y = event.motion.y;
	}

	void OMouseButtonDown(SDL_Event& event)
	{
		switch (event.button.button)
		{
		case SDL_BUTTON_LEFT:
			_mouse_button_states[LEFT] = true;
			break;
		case SDL_BUTTON_MIDDLE:
			_mouse_button_states[MIDDLE] = true;
			break;
		case SDL_BUTTON_RIGHT:
			_mouse_button_states[RIGHT] = true;
			break;
		default:
			break;
		}
	}

	void OnMouseButtonUp(SDL_Event& event)
	{
		switch (event.button.button)
		{
		case SDL_BUTTON_LEFT:
			_mouse_button_states[LEFT] = false;
			break;
		case SDL_BUTTON_MIDDLE:
			_mouse_button_states[MIDDLE] = false;
			break;
		case SDL_BUTTON_RIGHT:
			_mouse_button_states[RIGHT] = false;
			break;
		default:
			break;
		}
}
#if defined(__cpp_concepts) && _HAS_CXX20
	template SDL_Window* Init<int>(const char*, int, int);
#endif
};