#pragma once

#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>

#include <SDL.h>
#include "gl3w.h"

#include "imgui.h"
//#include "imgui_impl_sdl_gl3.h"


class ImageWindow
{
public:
	inline ImageWindow() = delete;
	ImageWindow(const char* name);
	ImageWindow(const char* name, ImGuiWindowFlags windowFlags);
	ImageWindow(const char* name, ImGuiWindowFlags windowFlags, ImVec2 minSize, ImVec2 maxSize);
	ImageWindow(const char* name, ImVec2 minSize, ImVec2 maxSize);
	void DrawWindow(const cv::Mat& mat);


private:
	GLuint _textureID;
	cv::Mat _image;
	std::string _name;
	bool _showWindow = true;
	ImVec2 _minSize;
	ImVec2 _maxSize;
	ImGuiWindowFlags _windowFlags = 0;
};

