#pragma once

#include "FilterStructures.h"

class MedicalInfoWindow
{
public:

	inline auto GetWindowName() noexcept -> const char* const
	{
		return _window_name.c_str();
	}
	inline auto GetFilePath() noexcept -> const char* const
	{
		return _dicom_file_path.c_str();
	}

	// OpenCV Mat
	cv::Mat mat_img_to_show; // Mat of the state we are modifying now -> should not be modified in any case
	cv::Mat mat_img_modified_to_show; // Apply all modification on this Mat
	cv::Mat mat_img_modified_to_show_last_modification;
	
	// The definition of these types can be found in "FilterStructures.h"
	BasicThresholding thresholding;
	ImageScaling scaling;
	HoughC houghcircles;
	CannyEdgeDetection canny;
	GradientEdgeDetection grad;
	Erode erosion;
	Dilate dilation;
	Open open;
	Close close;

	bool info_window_enabled_ = false;
	bool _open_dicom_successful = false;

	MedicalInfoWindow(const char* file_path);
	auto DrawWindow() -> void;
	auto PreCallSetup() -> void;
	inline auto PrintAllData(std::ostream& out) -> void const { _dataset->print(out); }
	inline auto SetWindowAndImageWindowName(const char* window_name, const char* image_window_name) -> void
	{
		_window_name = window_name;
		_window_filter_name = std::string(window_name) + " Filters";
		_image_window_name = image_window_name;
		_image_modified_window_name = image_window_name;
		_image_modified_window_name.append("Modified");
	}
private:
	auto DrawFiltersWindow() -> void;
	auto ApplyScaling() -> void;
	auto DrawInfoWindowInternal() -> void;
	auto FilterDecision() -> void;

	// filter storage
	typedef void(*filter_func_ptr)(MedicalInfoWindow*);
	std::vector<filter_func_ptr> _filter_function_pointers;


	std::string _dicom_file_path;
	// Modifiers
	bool _live_update = false;
	// DICOM
	DcmFileFormat _file_format;
	OFCondition _status;
	std::unique_ptr<DcmDataset> _dataset;
	OFString _current_ofstring;
	OFCondition _current_ofcondition;
	std::unique_ptr<DicomImage> _dicom_img;
	
	// Windowing stuff
	std::string _window_name;
	std::string _window_filter_name;
	std::string _image_window_name;
	std::string _image_modified_window_name;

	bool _pre_call_setup_ran = false;
	bool _image_window_enabled = false;
	bool _image_modified_window_enabled = false;
	bool filter_window_enabled_ = false;

	// Textures
	GLuint _original_image_texture; // Texture of the state we are modifying now
	GLuint _modified_image_texture; // Apply all modification on this Texture

	// data window
	void DrawDicomInfoWindow();
	bool _dicom_data_enabled = false;
	std::string _dicom_all_data;
};