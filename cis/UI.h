#pragma once

namespace UI
{
	// C++20 feature, only used if it is supported
#if defined(__cpp_concepts) && _HAS_CXX20
	template<typename T>
	concept integer = std::is_integral_v<T>;
	template<integer Int>
	SDL_Window* Init(const char* windowname, Int width, Int height);
#else
	SDL_Window* Init(const char* windowname, int width, int height);
#endif
	/// <summary>
	/// Clears color bit with supplied clear color and renders the SDL the window
	/// </summary>
	void RenderWindow(SDL_Window* window, ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f));
	/// <summary>
	/// Converts OpenCV Mat image (RGB) into OpenGL texture and returns the generated texture ID
	/// </summary>
	GLuint MatToTex(const cv::Mat& mat);
	/// <summary>
	/// Converts OpenCV Mat image (single channel, swizzle mask) into OpenGL texture and returns the generated texture ID
	/// </summary>
	GLuint MatToTex1C(const cv::Mat& mat);
	/// <summary>
	/// Cleans up ImGui and Destroys the window
	/// </summary>
	void Cleanup(SDL_Window* window);
	/// <summary>
	/// swizzle mask used for single channel images
	/// </summary>
	extern GLint _swizzle_mask[];

	// Event handling
	enum MouseButtons
	{
		LEFT = 0,
		MIDDLE = 1,
		RIGHT = 2
	};

	extern std::vector<bool> _mouse_button_states;
	extern cv::Point _mouse_pos;

	extern ImGuiIO* _io;

	void OnMouseMotion(SDL_Event& event);
	void OMouseButtonDown(SDL_Event& event);
	void OnMouseButtonUp(SDL_Event& event);

};

