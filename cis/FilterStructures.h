#pragma once

struct ImageScaling
{
	cv::Size _scaling;
	int _scalar = 10;
	bool _scaling_enabled = false;
};
// basic thresholding
struct BasicThresholding
{
	bool _thresholding = false;
	int _threshold_value = 0;
	int _threshold_type = 3;
};

// Hough Circles
struct HoughC
{
	bool enabled = false;

	std::vector<cv::Vec3f> circles = {};
	// everything should be a double here
	// rasterization size
	float dp = 1;
	// mat.rows/min_dist_divisor -> minimum distane between the circle centers
	float min_dist_divisor = 16;
	//Upper threshold for the internal Canny edge detector.
	float param_1 = 200;
	//Threshold for center detection.
	float param_2 = 100;
	//Minimum radius to be detected.If unknown, put zero as default.
	float min_radius = 0;
	//Maximum radius to be detected.If unknown, put zero as default.
	float max_radius = 0;
};

// Canny Edge Detection
struct CannyEdgeDetection
{
	bool enabled = false;
	float lowThreshold = 0;
};

// Gradient Edge Detection
struct GradientEdgeDetection
{
	bool enabled = false;
	float ksize = -1;
	float scale = 2;
	float delta = 3;
};

// Erode
struct Erode
{
	bool enabled = false;
	int erosion_type = 1;
	float size = 1;
};

// Dilate
struct Dilate
{
	bool enabled = false;
	int dilation_type = 1;
	float size = 1;
};

// Open
struct Open
{
	bool enabled = false;
	int dilation_type = 1;
	float size = 1;
};

// Close
struct Close
{
	bool enabled = false;
	int dilation_type = 1;
	float size = 1;
};