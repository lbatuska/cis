#include "pch.h"

#include "MedicalInfoWindow.h"
#include "Filters.h"

MedicalInfoWindow::MedicalInfoWindow(const char* file_path)
{
	_dicom_file_path = file_path;
	DcmFileFormat file_format;
	auto status = file_format.loadFile(_dicom_file_path.c_str());
	if (status.bad()) {
		std::cerr << "Problem openning file : " << _dicom_file_path << "\n";
		return;
	}
	_open_dicom_successful = true; // Inited to false because in case of faliure we return before changing it
	//dataset = file_format.getDataset(); don't use this
	_dataset.reset(file_format.getAndRemoveDataset()); // this one also obtains ownership of the DcmDataset from the DcmFileFormat

	_dicom_img = std::make_unique<DicomImage>(file_path);

	//_dataset->findAndGetFloat64(DCM_WindowCenter/*DcmTagKey(0x0010, 0x1050)*/, _window_original_center); // typedef double Float64 cuz everyone uses their own types
	//_dataset->findAndGetFloat64(DCM_WindowWidth/*DcmTagKey(0x0010, 0x1051)*/, _window_original_width);
	uchar* pixelData = (uchar*)(_dicom_img->getOutputData(8));
	mat_img_to_show = cv::Mat(int(_dicom_img->getHeight()), int(_dicom_img->getWidth()), CV_8U, pixelData);
	mat_img_to_show.copyTo(mat_img_modified_to_show);
	mat_img_to_show.copyTo(mat_img_modified_to_show_last_modification);
}

auto MedicalInfoWindow::DrawWindow() -> void
{
	if (info_window_enabled_)
	{
		if (!_pre_call_setup_ran)
		{
			PreCallSetup();
		}

		// Info Window (temporarily filter window as well) -> more or less separated already
		DrawInfoWindowInternal();



		// Original Image Window
		if (_image_window_enabled)
		{
			// No need to always update the texture, mat_img_to_show should never change
			if (_original_image_texture == NULL)
			{
				_original_image_texture = UI::MatToTex1C(mat_img_to_show);
			}

			ImGui::Begin(_image_window_name.c_str(), &_image_window_enabled, ImGuiWindowFlags_HorizontalScrollbar);
			ImGui::Image((void*)_original_image_texture, ImVec2(mat_img_to_show.size().width, mat_img_to_show.size().height));
			ImGui::End();
		}
		else
		{
			if (_original_image_texture != NULL)
			{
				glDeleteTextures(1, &_original_image_texture);
				_original_image_texture = NULL;
			}
		}

		// Modified Image Window
		if (_image_modified_window_enabled)
		{
			FilterDecision();

			if (_live_update)
			{
				// Only update the image shown if it has changed since last draw
				//if (!std::equal(mat_img_modified_to_show.begin<uchar>(), mat_img_modified_to_show.end<uchar>(), mat_img_modified_to_show_last_modification.begin<uchar>()))// not so performant
				if (cv::norm(mat_img_modified_to_show, mat_img_modified_to_show_last_modification, cv::NORM_L1))// better performance, check if they are the same
				{
					// always clear non null textures
					if (_modified_image_texture != NULL)
					{
						glDeleteTextures(1, &_modified_image_texture);
					}
					_modified_image_texture = UI::MatToTex1C(mat_img_modified_to_show);

					mat_img_modified_to_show.copyTo(mat_img_modified_to_show_last_modification);
				}
			}

			ImGui::Begin(_image_modified_window_name.c_str(), &_image_modified_window_enabled, ImGuiWindowFlags_HorizontalScrollbar);
			ImGui::Image((void*)_modified_image_texture, ImVec2(mat_img_modified_to_show.size().width, mat_img_modified_to_show.size().height));
			ImGui::End();
		}
	}

}

auto MedicalInfoWindow::DrawInfoWindowInternal() -> void
{
	ImGui::Begin(_window_name.c_str(), &info_window_enabled_);

	ImGui::Checkbox("Show DICOM Info", &_dicom_data_enabled);
	if (_dicom_data_enabled)
	{
		DrawDicomInfoWindow();
	}

	ImGui::Checkbox("Show DICOM Image", &_image_window_enabled);
	ImGui::Checkbox("Show Modified DICOM Image", &_image_modified_window_enabled);


	ImGui::Checkbox("Scaling (always uses original image then applies all other modifications)", &scaling._scaling_enabled);
	ImGui::SliderInt("Scaling (n/10.0f)", &scaling._scalar, 1, 20);

	if (ImGui::Button("Apply Scaling (always take the original image)"))
	{
		ApplyScaling();
	}

	ImGui::Checkbox("LiveUpdate (can be performance heavy in case of multiple DICOM images)", &_live_update);
	if (!_live_update)
	{
		if (ImGui::Button("Apply Changes"))
		{
			if (_image_modified_window_enabled)
			{
				FilterDecision();

				// always clear non null textures
				if (_modified_image_texture != NULL)
				{
					glDeleteTextures(1, &_modified_image_texture);
				}
				_modified_image_texture = UI::MatToTex1C(mat_img_modified_to_show);
			}
		}
	}

	ImGui::Checkbox("Filters window", &filter_window_enabled_);
	if (filter_window_enabled_)
	{
		DrawFiltersWindow();
	}

	if (ImGui::Button("Reset (Drop All Changes)"))
	{
		mat_img_to_show.copyTo(mat_img_modified_to_show);
		mat_img_to_show.copyTo(mat_img_modified_to_show_last_modification);
		if (_modified_image_texture != NULL)
		{
			glDeleteTextures(1, &_modified_image_texture);
		}
		_modified_image_texture = UI::MatToTex1C(mat_img_modified_to_show);
	}

	ImGui::End();
}

auto MedicalInfoWindow::DrawFiltersWindow() -> void
{
	ImGui::Begin(_window_filter_name.c_str(), &filter_window_enabled_);
	// Basic thresholding --------------------------------------------------------------------------------------------
	if (!thresholding._thresholding)
	{
		if (ImGui::Button("Add Basic Thresholding"))
		{
			_filter_function_pointers.push_back(Filters::BasicThresholdingFunc);
			thresholding._thresholding = true;
		}
	}
	else
	{
		if (ImGui::Button("Remove Basic Thresholding"))
		{
			std::vector<filter_func_ptr>::iterator it = std::find(_filter_function_pointers.begin(),
				_filter_function_pointers.end(), Filters::BasicThresholdingFunc);
			_filter_function_pointers.erase(it);
			thresholding._thresholding = false;
		}
		ImGui::Text("0 - Binary, 1 - Binary Inverted, 2 - Truncate, 3 - To Zero, 4 - To Zero Inverted");
		ImGui::SliderInt("Type", &thresholding._threshold_type, 0, 4);
		ImGui::SliderInt("Value", &thresholding._threshold_value, 0, 255);
	}
	// Basic thresholding --------------------------------------------------------------------------------------------


	// Histogram
	if (ImGui::Button("Histogram Plot"))
	{
		// Filters::Histogram(this, true);
		Filters::Segmentation(this);
	}

	// Hough Circles --------------------------------------------------------------------------------------------
	ImGui::Checkbox("Hough Circles", &houghcircles.enabled);
	if (houghcircles.enabled)
	{
		ImGui::Text("min dist -> img.rows/n");
		ImGui::SliderFloat("dist_div", &houghcircles.min_dist_divisor, 1, 100);
		ImGui::Text("Upper threshold for the internal Canny edge detector");
		ImGui::SliderFloat("param_1", &houghcircles.param_1, 1, 500);
		ImGui::Text("Threshold for center detection");
		ImGui::SliderFloat("param_2", &houghcircles.param_2, 1, 100);
		ImGui::Text("Minimum radius to be detected.If unknown, put zero as default");
		ImGui::SliderFloat("min_radius", &houghcircles.min_radius, 0, 1000);
		ImGui::Text("Maximum radius to be detected.If unknown, put zero as default");
		ImGui::SliderFloat("max_radius", &houghcircles.max_radius, 0, 1000);
		if (ImGui::Button("Do Hough Circles"))
		{
			Filters::HoughCircles(this);
		}
	}
	// Hough Circles --------------------------------------------------------------------------------------------

	// Histogram EQ ------------------------------------------------------------------
	if (ImGui::Button("Histogram Equalization"))
	{
		Filters::EqualizeHistogram(this);
	}
	// Histogram EQ ------------------------------------------------------------------

	// Canny Edge Detection ------------------------------------------------------------------
	ImGui::Checkbox("Canny Edge Detection", &canny.enabled);
	if (canny.enabled)
	{
		ImGui::SliderFloat("threshold", &canny.lowThreshold, 0, 100);
		if (ImGui::Button("Apply"))
		{
			Filters::CannyEdgeDetection(this);
		}
	}
	// Canny Edge Detection ------------------------------------------------------------------

	// Gradient Edge Detection ------------------------------------------------------------------
	ImGui::Checkbox("Gradient Edge Detection", &grad.enabled);
	if (grad.enabled)
	{
		ImGui::SliderFloat("ksize", &grad.ksize, -1, 29);
		ImGui::SliderFloat("scale", &grad.scale, 0, 100);
		ImGui::SliderFloat("delta", &grad.delta, 0, 100);
		if (ImGui::Button("Apply"))
		{
			Filters::GradientEdgeDetection(this);
		}
	}
	// Gradient Edge Detection ------------------------------------------------------------------

	// Erosion ------------------------------------------------------------------
	ImGui::Checkbox("Erosion", &erosion.enabled);
	if (erosion.enabled)
	{
		ImGui::Text("1 -> MORPH_RECT, 2 -> MORPH_CROSS, 3 -> MORPH_ELLIPSE/n");
		ImGui::SliderInt("Erosion Type", &erosion.erosion_type, 1, 3);
		ImGui::SliderFloat("Size", &erosion.size, 0, 50);
		if (ImGui::Button("Apply"))
		{
			Filters::Erode(this);
		}
	}
	// Erosion ------------------------------------------------------------------

	// Dilation ------------------------------------------------------------------
	ImGui::Checkbox("Dilation", &dilation.enabled);
	if (dilation.enabled)
	{
		ImGui::Text("1 -> MORPH_RECT, 2 -> MORPH_CROSS, 3 -> MORPH_ELLIPSE/n");
		ImGui::SliderInt("Dilation Type", &dilation.dilation_type, 1, 3);
		ImGui::SliderFloat("Size", &dilation.size, 0, 50);
		if (ImGui::Button("Apply"))
		{
			Filters::Dilate(this);
		}
	}
	// Dilation ------------------------------------------------------------------

	// Open ------------------------------------------------------------------
	ImGui::Checkbox("Open", &open.enabled);
	if (open.enabled)
	{
		ImGui::Text("1 -> MORPH_RECT, 2 -> MORPH_CROSS, 3 -> MORPH_ELLIPSE/n");
		ImGui::SliderInt("Dilation Type", &open.dilation_type, 1, 3);
		ImGui::SliderFloat("Size", &open.size, 0, 50);
		if (ImGui::Button("Apply"))
		{
			Filters::Open(this);
		}
	}
	// Open ------------------------------------------------------------------

	// Close ------------------------------------------------------------------
	ImGui::Checkbox("Close", &close.enabled);
	if (close.enabled)
	{
		ImGui::Text("1 -> MORPH_RECT, 2 -> MORPH_CROSS, 3 -> MORPH_ELLIPSE/n");
		ImGui::SliderInt("Dilation Type", &close.dilation_type, 1, 3);
		ImGui::SliderFloat("Size", &close.size, 0, 50);
		if (ImGui::Button("Apply"))
		{
			Filters::Close(this);
		}
	}
	// Close ------------------------------------------------------------------

	ImGui::End();
}

auto MedicalInfoWindow::FilterDecision() -> void
{
	size_t sz = _filter_function_pointers.size();
	if (sz < 1)
	{
		return;
	}
	//in the end run through and call all filters
	for (int i = 0; i < sz; i++)
	{
		_filter_function_pointers[i](this);
	}
}

void MedicalInfoWindow::DrawDicomInfoWindow()
{
	if (_dicom_all_data.empty())
	{
		std::stringstream oss;
		_dataset->print(oss);
		_dicom_all_data = oss.str();
	}
	ImGui::Begin("DICOM content", &_dicom_data_enabled, ImGuiWindowFlags_HorizontalScrollbar);
	ImGui::TextUnformatted(_dicom_all_data.c_str());
	ImGui::End();
}

auto MedicalInfoWindow::ApplyScaling() -> void
{
	//_mat_img.copyTo(mat_img_to_show);

	if (scaling._scaling_enabled)
	{
		// Scaling should always use the unmodified Mat img
		float scr = (scaling._scalar / 10.0f);
		scaling._scaling = cv::Size(mat_img_to_show.cols * scr, mat_img_to_show.rows * scr);
		//MatimgModifiedToShow = cv::Mat(Scaling, CV_8U);
		cv::resize(mat_img_to_show, mat_img_modified_to_show, scaling._scaling);

		mat_img_modified_to_show.copyTo(mat_img_modified_to_show_last_modification);
		// always clear non null textures
		if (_modified_image_texture != NULL)
		{
			glDeleteTextures(1, &_modified_image_texture);
		}
		_modified_image_texture = UI::MatToTex1C(mat_img_modified_to_show);
	}
}

auto MedicalInfoWindow::PreCallSetup() -> void
{
	if (_window_name.empty() || _image_window_name.empty())
	{
		_window_name = { "MedicalImageInfoWindow" };
		_window_filter_name = { "MedicalImageInfoWindow Filters" };
		_image_window_name = { "MedicalImageWindow" };
		_image_modified_window_name = { "MedicalImageWindowModified" };
	}
	_pre_call_setup_ran = true;
}