#include "ImageWindow.h"

#include "UI.h"

ImageWindow::ImageWindow(const char* name)
{
	_name = name;
}

ImageWindow::ImageWindow(const char* name, ImGuiWindowFlags windowFlags) :ImageWindow(name)
{
	_windowFlags = windowFlags;
}

ImageWindow::ImageWindow(const char* name, ImGuiWindowFlags windowFlags, ImVec2 minSize, ImVec2 maxSize) : ImageWindow(name, minSize, maxSize)
{
	_windowFlags = windowFlags;
}

ImageWindow::ImageWindow(const char* name, ImVec2 minSize, ImVec2 maxSize) : ImageWindow(name)
{
	_minSize = minSize;
	_maxSize = maxSize;
}

void ImageWindow::DrawWindow(const cv::Mat& mat)
{
	glDeleteTextures(1, &_textureID);
	_textureID = UI::MatToTex(mat);
	if (_minSize.x != 0 && _minSize.y != 0 && _maxSize.x > _minSize.x&& _maxSize.y > _minSize.y)
	{
		ImGui::SetNextWindowSizeConstraints(_minSize, _maxSize);
	}
	ImGui::Begin(_name.c_str(), &_showWindow, _windowFlags);

	ImGui::Image((void*)_textureID, ImVec2(mat.size().width, mat.size().height));

	ImGui::End();
}
