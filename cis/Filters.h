#pragma once

#include "MedicalInfoWindow.h"

namespace Filters
{
	auto BasicThresholdingFunc(MedicalInfoWindow* ptr) -> void;
	auto HoughCircles(MedicalInfoWindow* ptr) -> void;
	auto Histogram(MedicalInfoWindow* ptr) -> void;
	auto Segmentation(MedicalInfoWindow* ptr) -> void;
	auto ImageEnhancement(MedicalInfoWindow* ptr) -> void;
	auto EqualizeHistogram(MedicalInfoWindow* ptr) -> void;
	auto CannyEdgeDetection(MedicalInfoWindow* ptr) -> void;
	auto GradientEdgeDetection(MedicalInfoWindow* ptr) -> void;
	auto Dilate(MedicalInfoWindow* ptr) -> void;
	auto Erode(MedicalInfoWindow* ptr) -> void;
	auto Open(MedicalInfoWindow* ptr) -> void;
	auto Close(MedicalInfoWindow* ptr) -> void;
};