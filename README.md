# Introduction to Computer Integrated Surgery Project
All necessary libraries are precompiled and included in the project  
Minimum OpenGL version required 3.0
## OpenCV 4.2.0
For image processing  
https://opencv.org/
## SDL2 2.0.10
Wrapper library to access operating-system-specific functions  
https://www.libsdl.org/index.php
## OpenGL
For rendering the processed OpenCV image  
https://www.opengl.org/
## gl3w: Simple OpenGL core profile loading
For loading core profile OpenGL functionalities  
https://github.com/skaslev/gl3w
## DCMTK 3.6.5
A colection of libraries to handle DICOM files and communication  
https://github.com/DCMTK/dcmtk  
Compiled with MSVC  
## tinyfiledialogs
https://github.com/native-toolkit/tinyfiledialogs  
  
![](cis.png)